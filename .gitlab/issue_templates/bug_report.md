**Describe the bug**

A clear and concise description of what the bug is.

**Steps To Reproduce**
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'

**Expected behavior**

A clear and concise description of what you expected to happen.

**Actual behavior**

A clear and concise description of what actually happened.

**Screenshots**

If applicable, add screenshots to help explain your problem.

**Further Information**
 - Device: [e.g. Samsung Galaxy A54]
 - OS: [e.g. Android 13]
 - Version [e.g. 0.2.0]

Add any other context about the problem here.
