import 'package:journeyplanner_fl/data/layover.dart';
import 'package:journeyplanner_fl/data/linerun.dart';
import 'package:journeyplanner_fl/data/station.dart';

class Leg extends LineRun {
  final _beforeSection = <Layover>[];
  final _afterSection = <Layover>[];

  Iterable<Layover> get beforeSection => _beforeSection;
  Iterable<Layover> get afterSection => _beforeSection;

  Leg(LineRun run, Station start, Station end)
      : super(run.id, run.lineName, run.product) {
    var hitStart = false;
    var hitEnd = false;
    final activeSection = <Layover>[];
    for (var value in run.layovers) {
      if (value.station.id == start.id) {
        hitStart = true;
      }
      if (hitEnd) {
        _afterSection.add(value);
      } else if (hitStart) {
        activeSection.add(value);
      } else {
        _beforeSection.add(value);
      }
      if (value.station.id == end.id) {
        hitEnd = true;
      }
    }
    complete(activeSection);
  }
}
