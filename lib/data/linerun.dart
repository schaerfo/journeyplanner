// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Christian Schärf

import 'package:journeyplanner_fl/data/product.dart';

import 'layover.dart';

class LineRun {
  final _layovers = <Layover>[];
  var _completed = false;

  get isCompleted => _completed;
  Iterable<Layover> get layovers => _layovers;
  Layover get origin => _layovers.first;
  Layover get destination => _layovers.last;

  String id;
  String lineName;
  Product product;

  LineRun(this.id, this.lineName, this.product);

  LineRun.fromEndpoints(this.id, this.lineName, this.product, Layover origin,
      Layover destination) {
    _layovers.addAll([origin, destination]);
  }

  void complete(Iterable<Layover> layovers) {
    _layovers.clear();
    _layovers.addAll(layovers);
    _completed = true;
  }
}
