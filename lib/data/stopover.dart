// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Christian Schärf

import 'package:journeyplanner_fl/data/station.dart';

import 'linerun.dart';

abstract class Stopover {
  // A stopover is when you are at a station and a train stops near you.
  LineRun run;
  Station station;

  Stopover(this.run, this.station);

  String where();
  DateTime scheduledWhen();
}

class Departure extends Stopover {
  final DateTime scheduledDeparture;
  final String destination;

  Departure(
      super.run, super.station, this.scheduledDeparture, this.destination);

  @override
  DateTime scheduledWhen() {
    return scheduledDeparture;
  }

  @override
  String where() {
    return destination;
  }
}

class Arrival extends Stopover {
  final DateTime scheduledArrival;
  final String origin;

  Arrival(super.run, super.station, this.scheduledArrival, this.origin);

  @override
  DateTime scheduledWhen() {
    return scheduledArrival;
  }

  @override
  String where() {
    return origin;
  }
}
