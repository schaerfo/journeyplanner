// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Christian Schärf

import 'package:flutter/material.dart';

import 'package:intl/intl.dart' as intl;
import 'package:journeyplanner_fl/data/product.dart';
import 'package:journeyplanner_fl/data/stopover.dart';

import '../backend/db_transport_rest.dart';
import '../data/leg.dart';
import '../data/linerun.dart';
import '../data/station.dart';

class LineDisplay extends StatefulWidget {
  const LineDisplay(
      {super.key, required this.run, this.start, this.end, this.onLegSelected});

  final LineRun run;
  final Station? start;
  final Station? end;
  final Function(Leg)? onLegSelected;

  @override
  State<LineDisplay> createState() => _LineDisplayState();

  Widget title() {
    return Text(run.lineName);
  }
}

class _LineDisplayState extends State<LineDisplay> {
  final _backend = DbTransportRestBackend();
  Station? _entry;

  void _fetchLineRun() async {
    await _backend.fetchLineRun(widget.run);
    setState(() {});
  }

  IconData getIconForProduct() {
    switch (widget.run.product) {
      case Product.bus:
        return Icons.directions_bus;
      case Product.tram:
        return Icons.tram;
      case Product.metro:
        return Icons.subway;
      case Product.suburban:
        return Icons.directions_transit;
      case Product.longDistance:
      case Product.highSpeed:
        return Icons.directions_train;
      default:
        return Icons.train;
    }
  }

  @override
  Widget build(BuildContext context) {
    final icon = Icon(getIconForProduct());

    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        leading: icon,
        title: widget.title(),
        onExpansionChanged: (value) {
          if (value) {
            _fetchLineRun();
          }
        },
        children: widget.run.isCompleted
            ? _layoverListTiles(context)
            : <Widget>[const CircularProgressIndicator()],
      ),
    );
  }

  DateTime? extractDate(DateTime? dt) {
    if (dt == null) {
      return null;
    } else {
      return DateTime(dt.year, dt.month, dt.day);
    }
  }

  List<Widget> _layoverListTiles(BuildContext context) {
    final result = <Widget>[];

    DateTime? lastDate;
    bool active = widget.start == null && _entry == null;
    for (final currStopover in widget.run.layovers) {
      if (currStopover.scheduledDeparture != null &&
          extractDate(currStopover.scheduledDeparture) != lastDate) {
        lastDate = extractDate(currStopover.scheduledDeparture);
        result.add(Text(
          intl.DateFormat.yMEd().format(currStopover.scheduledDeparture!),
        ));
      }

      if (currStopover.station.id == widget.start?.id ||
          currStopover.station == _entry) {
        active = true;
      }
      Widget? entryExitButton;
      if (widget.onLegSelected != null && active) {
        if (currStopover.station.id == widget.start?.id) {
          entryExitButton = const Icon(Icons.login);
        } else if (currStopover.station.id == widget.end?.id) {
          entryExitButton = const Icon(Icons.logout);
        } else if (widget.start == null && _entry == null ||
            currStopover.station == _entry) {
          entryExitButton = IconButton(
            onPressed: () {
              _setEntry(currStopover.station);
            },
            icon: const Icon(Icons.login),
          );
        } else {
          entryExitButton = IconButton(
            onPressed: () {
              _setExit(currStopover.station);
            },
            icon: const Icon(Icons.logout),
          );
        }
      }
      result.add(ListTile(
        leading: currStopover.scheduledDeparture == null
            ? null
            : Text(
                intl.DateFormat.Hm().format(currStopover.scheduledDeparture!),
              ),
        title: Text(
          currStopover.station.name,
          style: active ? null : const TextStyle(color: Colors.black45),
        ),
        trailing: entryExitButton,
      ));
      if (currStopover.station.id == widget.end?.id && active) {
        active = false;
      }
    }
    return result;
  }

  void _setEntry(Station entry) {
    if (widget.end != null) {
      widget.onLegSelected!(Leg(widget.run, entry, widget.end!));
    } else {
      setState(() {
        if (_entry == null) {
          _entry = entry;
        } else {
          _entry = null;
        }
      });
    }
  }

  void _setExit(Station exit) {
    if (widget.start != null) {
      widget.onLegSelected!(Leg(widget.run, widget.start!, exit));
    } else {
      widget.onLegSelected!(Leg(widget.run, _entry!, exit));
    }
  }
}

class StopoverDisplay extends LineDisplay {
  final Stopover stopover;

  StopoverDisplay(
      {super.key,
      required this.stopover,
      super.start,
      super.end,
      super.onLegSelected})
      : super(run: stopover.run);

  @override
  Widget title() {
    final lineName = stopover.run.lineName;
    final text = stopover.where();
    final time = stopover.scheduledWhen();
    return Text('${intl.DateFormat.Hm().format(time)} $lineName $text');
  }
}
