// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2023 Christian Schärf

import 'package:flutter/material.dart';

import 'package:intl/intl.dart' as intl;

import '../data/connection.dart';

class DateTimeSelection extends StatefulWidget {
  final DateTime dateTime;
  final Connection? connection;

  const DateTimeSelection(this.dateTime, {super.key, this.connection});

  @override
  State<DateTimeSelection> createState() => _DateTimeSelectionState();
}

class _DateTimeSelectionState extends State<DateTimeSelection> {
  late DateTime dateTime;

  @override
  void initState() {
    super.initState();
    dateTime = widget.dateTime;
  }

  @override
  Widget build(BuildContext context) {
    final timeComparator = widget.connection != null
        ? (widget.connection!.type == ConnectionType.fromHere
            ? 'later'
            : 'earlier')
        : null;
    final formattedTimeThreshold = widget.connection != null
        ? intl.DateFormat.Hm().format(widget.connection!.when)
        : null;
    return Column(
      children: [
        if (widget.connection != null)
          Text('Select times $timeComparator than $formattedTimeThreshold'),
        ListTile(
          leading: const Icon(Icons.calendar_month),
          title: Text(intl.DateFormat.yMEd().format(dateTime)),
          onTap: () async {
            final newDate = await showDatePicker(
              context: context,
              initialDate: dateTime,
              firstDate: _periodStart(),
              lastDate: _periodEnd(),
            );
            if (newDate == null) {
              return;
            }
            setState(() {
              dateTime = dateTime.copyWith(
                year: newDate.year,
                month: newDate.month,
                day: newDate.day,
              );
            });
          },
        ),
        ListTile(
          leading: const Icon(Icons.access_time),
          title: Text(intl.DateFormat.Hm().format(dateTime)),
          onTap: () async {
            final newTime = await showTimePicker(
                context: context,
                initialTime: TimeOfDay.fromDateTime(dateTime));
            if (newTime == null) {
              return;
            }
            final newDateTime = dateTime.copyWith(
              hour: newTime.hour,
              minute: newTime.minute,
            );
            if (_isSelectedDateTimeValid(newDateTime)) {
              setState(() {
                dateTime = newDateTime;
              });
            } else if (context.mounted) {
              // FIXME snack bar is displayed under bottom sheet
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                      'You have to select a time $timeComparator than $formattedTimeThreshold'),
                ),
              );
            }
          },
        ),
        Wrap(
          spacing: 5,
          children: timeChips(),
        ),
        TextButton(
            onPressed: () {
              Navigator.pop(context, dateTime);
            },
            child: const Text('OK')),
      ],
    );
  }

  DateTime _periodStart() {
    if (widget.connection != null &&
        widget.connection!.type == ConnectionType.fromHere) {
      return widget.connection!.when;
    } else {
      return DateTime.parse('2020-01-01');
    }
  }

  DateTime _periodEnd() {
    if (widget.connection != null &&
        widget.connection!.type == ConnectionType.toHere) {
      return widget.connection!.when;
    } else {
      return DateTime.parse('2029-12-31');
    }
  }

  bool _isSelectedDateTimeValid(DateTime dateTime) {
    final equalOrAfterPeriodStart = dateTime.compareTo(_periodStart()) >= 0;
    final equalOrBeforePeriodEnd = dateTime.compareTo(_periodEnd()) <= 0;
    return equalOrAfterPeriodStart && equalOrBeforePeriodEnd;
  }

  List<ActionChip> timeChips() {
    final chips = () {
      if (widget.connection == null) {
        return [
          (
            const Text('Now'),
            DateTime.now(),
          ),
          (
            const Text('In 15 Minutes'),
            DateTime.now().add(const Duration(minutes: 15)),
          ),
          (
            const Text('In 1 Hour'),
            DateTime.now().add(const Duration(hours: 1)),
          ),
          (
            const Text('Tomorrow'),
            DateTime.now().add(const Duration(days: 1)),
          ),
        ];
      } else {
        final connectionTime = widget.connection!.when;
        final combFunc = widget.connection!.type == ConnectionType.fromHere
            ? (Duration delta) {
                return connectionTime.add(delta);
              }
            : (Duration delta) {
                return connectionTime.subtract(delta);
              };
        return [
          (
            const Text('Immediate transfer'),
            connectionTime,
          ),
          (
            const Text('15 minute transfer'),
            combFunc(const Duration(minutes: 15)),
          ),
          (
            const Text('1 hour transfer'),
            combFunc(const Duration(hours: 1)),
          ),
        ];
      }
    }();

    final result = [
      for (final chip in chips) ...[
        ActionChip(
          label: chip.$1,
          onPressed: () {
            Navigator.pop(context, chip.$2);
          },
        )
      ]
    ];
    return result;
  }
}
