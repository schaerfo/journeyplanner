import 'package:flutter/material.dart';

import 'package:intl/intl.dart' as intl;

import '../data/layover.dart';
import '../data/leg.dart';
import '../data/product.dart';

class LegDisplay extends StatelessWidget {
  final Leg leg;

  const LegDisplay({super.key, required this.leg});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        leading: Icon(getIconForProduct()),
        title: _title(),
        children: _layovers(),
      ),
    );
  }

  IconData getIconForProduct() {
    switch (leg.product) {
      case Product.bus:
        return Icons.directions_bus;
      case Product.tram:
        return Icons.tram;
      case Product.metro:
        return Icons.subway;
      case Product.suburban:
        return Icons.directions_transit;
      case Product.longDistance:
      case Product.highSpeed:
        return Icons.directions_train;
      default:
        return Icons.train;
    }
  }

  Widget _title() {
    final departureTime =
        intl.DateFormat.Hm().format(leg.origin.scheduledDeparture!);
    final departureStation = leg.origin.station.name;
    final arrivalTime =
        intl.DateFormat.Hm().format(leg.destination.scheduledArrival!);
    final arrivalStation = leg.destination.station.name;
    return Text(
      '${leg.lineName} $departureTime $departureStation - $arrivalTime $arrivalStation',
    );
  }

  List<Widget> _layovers() {
    final result = <Widget>[];
    for (final currLayover in leg.beforeSection) {
      result.add(_LayoverDisplay(
        currLayover,
        active: false,
      ));
    }
    for (final currLayover in leg.layovers) {
      result.add(_LayoverDisplay(currLayover));
    }
    for (final currLayover in leg.afterSection) {
      result.add(_LayoverDisplay(
        currLayover,
        active: false,
      ));
    }
    return result;
  }
}

class _LayoverDisplay extends StatelessWidget {
  final Layover layover;
  final bool active;

  const _LayoverDisplay(this.layover, {this.active = true});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: layover.scheduledDeparture == null
          ? null
          : Text(
              intl.DateFormat.Hm().format(layover.scheduledDeparture!),
            ),
      title: Text(
        layover.station.name,
        style: active ? null : const TextStyle(color: Colors.black45),
      ),
    );
  }
}
